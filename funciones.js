function inicializar(){
    let lienzo=document.getElementById("miLienzo");
    let ctx=lienzo.getContext('2d');
    lienzo.width=500;
    lienzo.height=500;
    
    ctx.beginPath();
    ctx.strokeStyle = 'green';
    ctx.arc(75,75,50,0,Math.PI*2,true); // Círculo externo
    ctx.moveTo(110,75);
    ctx.stroke();
    }